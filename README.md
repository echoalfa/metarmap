# METAR Map
## Installation
Install requirements with `pip install -r requirements.txt`

## Usage
- Start the Flask app with `sudo nohup python3 web_interface.py &`
- Open the browser & go to `127.0.0.1/metar_map_setup`

`work in progress`


## To do
### Priority 1
Have map boot in AP mode when no known WiFi network is available (SSID: Metarmap, KEY: 12345678). On connection automatically direct the user to http://192.168.10.53/metarmap/connectivity (or similar) without the need for username and password. Do not make other setup pages available. Could also request the user set the notification email address at this stage, that way it’s one less thing i have to set prior to shipping. On adding WiFi details and email save and reboot.

#### Tasks:
- Detect WiFi connection status
- Reliably switch to AP mode
- Web UI to capture WiFi details, email & password


### Priority 2
Startup Mailer to send email to user explaining how to connect to the map and make settings changes via http://192.168.10.53/metarmap or similar. This is on power cycle (not during re-boots where power is not lost). Although somehow an initial email needs to be sent when switching from AP mode to WiFi mode after initial setup?

#### Tasks:
- Distinguish between reboot & re-start
- 2 email templates
	- 'Getting started' for first boot since purchase (triggered by Connection to novel WiFi)
	- Power on (basic notification & link to web UI)


### Priority 3
Ability to push updates to maps running this version. These updates must not make changes to the "airports" file as this is specific to each build and never changes. They must also not change the settings associated with the legend as the offset will be different for each type of map.

#### Tasks:
- Set up public git repo with relevant files
- .gitignore to protect files specific to each user's map
- Option on UI to download & apply updates

```
# ----- Show a set of Legend LEDS at the end -----
SHOW_LEGEND = False # Set to true if you want to have a set of LEDs at the end show the legend
# You'll need to add 7 LEDs at the end of your string of LEDs
# If you want to offset the legend LEDs from the end of the last airport from the airports file,
# then change this offset variable by the number of LEDs to skip before the LED that starts the legend
OFFSET_LEGEND_BY = 0
# The order of LEDs is:
# VFR
# MVFR
# IFR
# LIFR
# LIGHTNING
# WINDY
# HIGH WINDS
```