#!/usr/bin/env python3
import json
import os
import urllib.request
import xml.etree.ElementTree as ET
import board
import neopixel
import time
import datetime
try:
    import astral
except ImportError:
    astral = None
try:
    import displaymetar
except ImportError:
    displaymetar = None

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

# metar.py script iteration 2.0.0
# webUI v1.0

# ---------------------------------------------------------------------------
# ------------DEFAULT CONFIGURATION-----------------------------------------
# ---------------------------------------------------------------------------

# --- NeoPixel LED Configuration ---
LED_COUNT       = 50
LED_PIN         = board.D18     # GPIO pin connected to the pixels (18 is PCM).
LED_ORDER       = neopixel.GRB  # Strip type and colour ordering

# --- Fixed settings ---
COLOR_CLEAR = [0, 0, 0]
FADE_INSTEAD_OF_BLINK   = True  # Set to False if you want blinking wind animations
ALWAYS_BLINK_FOR_GUSTS  = False # Always animate for Gusts (regardless of speeds)
BLINK_SPEED             = 1.0
BLINK_TOTALTIME_SECONDS = 300
ACTIVATE_EXTERNAL_METAR_DISPLAY = False     # Set to True if you want to display METAR conditions to a small external display
DISPLAY_ROTATION_SPEED = 5.0            # Float in seconds, e.g 2.0 for two seconds

# --- Legend ---
SHOW_LEGEND = True          # Set to true if you want to have a set of LEDs at the end show the legend
# You'll need to add 7 LEDs at the end of your string of LEDs
# If you want to offset the legend LEDs from the end of the last airport from the airports file,
# then change this offset variable by the number of LEDs to skip before the LED that starts the legend
OFFSET_LEGEND_BY = 0
# The order of LEDs is:
#   VFR
#   MVFR
#   IFR
#   LIFR
#   LIGHTNING
#   WINDY
#   HIGH WINDS

# --- Customer-controlled settings ---
CONFIGURATION = {
    'colours' : {
        'VFR':[225, 0, 0],
        'MVFR':[0, 0, 255],
        'IFR':[0, 255, 0],
        'LIFR':[0, 125, 125],
        'lightning':[255, 255, 255],
        'high_wind':[255, 255, 0]
    },
    'wind': {
        'enabled': True,
        'blink_threshold': 15,
        'high_wind_threshold': 25
    },
    'lightning': {
        'enabled': True
    },
    'brightness': {
        'day': 0.5,
        'night': 0.1
    },
    'dim_at_sunset': {
        'enabled': True,
        'location': 'London'
    }
}

# ---------------------------------------------------------------------------
# ------------END OF CONFIGURATION-------------------------------------------
# ---------------------------------------------------------------------------

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def fade_colour(original):
    faded = []
    for colour_value in original:
        if colour_value > 0:
            faded.append(int(colour_value / 2))
        else:
            faded.append(0)
    return tuple(faded)

def main():
    # Figure out sunrise/sunset times if astral is being used
    global astral
    if astral is not None and CONFIGURATION['dim_at_sunset']['enabled']:
        try:
            # For older clients running python 3.5 which are using Astral 1.10.1
            ast = astral.Astral()
            try:
                city = ast[CONFIGURATION['dim_at_sunset']['location']]
            except KeyError:
                print("Error: Location not recognized, please check list of supported cities and reconfigure")
            else:
                print(city)
                sun = city.sun(date = datetime.datetime.now().date(), local = True)
                bright_time_start = sun['sunrise'].time()
                dim_time_start = sun['sunset'].time()
        except AttributeError:
            # newer Raspberry Pi versions using Python 3.6+ using Astral 2.2
            import astral.geocoder
            import astral.sun
            try:
                city = astral.geocoder.lookup(CONFIGURATION['dim_at_sunset']['location'], astral.geocoder.database())
            except KeyError:
                print("Error: Location not recognized, please check list of supported cities and reconfigure")
            else:
                print(city)
                sun = astral.sun.sun(city.observer, date = datetime.datetime.now().date(), tzinfo=city.timezone)
                bright_time_start = sun['sunrise'].time()
                dim_time_start = sun['sunset'].time()
        print("Sunrise:" + bright_time_start.strftime('%H:%M') + " Sunset:" + dim_time_start.strftime('%H:%M'))
    
    else:
        bright_time_start = datetime.time(7,0)
        dim_time_start = datetime.time(19,0)

    # Initialize the LED strip
    bright = bright_time_start < datetime.datetime.now().time() < dim_time_start
    print("Wind animation:" + str(CONFIGURATION['wind']['enabled']))
    print("Lightning animation:" + str(CONFIGURATION['lightning']['enabled']))
    print("Daytime Dimming:" + str(CONFIGURATION['dim_at_sunset']['enabled']) )
    print("External Display:" + str(ACTIVATE_EXTERNAL_METAR_DISPLAY))
    pixels = neopixel.NeoPixel(LED_PIN, LED_COUNT, brightness = CONFIGURATION['brightness']['night'] if (CONFIGURATION['dim_at_sunset']['enabled'] and bright == False) else CONFIGURATION['brightness']['day'], pixel_order = LED_ORDER, auto_write = False)

    # Read the airports file to retrieve list of airports and use as order for LEDs
    with open(f"{THIS_DIRECTORY}airports") as f:
        airports = f.readlines()
    airports = [x.strip() for x in airports]
    try:
        with open(f"{THIS_DIRECTORY}displayairports") as f2:
            displayairports = f2.readlines()
        displayairports = [x.strip() for x in displayairports]
        print("Using subset airports for LED display")
    except IOError:
        print("Rotating through all airports on LED display")
        displayairports = None

    # Retrieve METAR from aviationweather.gov data server
    # Details about parameters can be found here: https://www.aviationweather.gov/dataserver/example?datatype=metar
    url = "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=5&mostRecentForEachStation=true&stationString=" + ",".join([item for item in airports if item != "NULL"])
    print(url)
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'})
    content = urllib.request.urlopen(req).read()

    # Retrieve flying conditions from the service response and store in a dictionary for each airport
    root = ET.fromstring(content)
    conditionDict = { "NULL": {"flightCategory" : "", "windDir": "", "windSpeed" : 0, "windGustSpeed" :  0, "windGust" : False, "lightning": False, "tempC" : 0, "dewpointC" : 0, "vis" : 0, "altimHg" : 0, "obs" : "", "skyConditions" : {}, "obsTime" : datetime.datetime.now() } }
    conditionDict.pop("NULL")
    stationList = []
    for metar in root.iter('METAR'):
        stationId = metar.find('station_id').text
        if metar.find('flight_category') is None:
            print("Missing flight condition, skipping.")
            continue
        flightCategory = metar.find('flight_category').text
        windDir = ""
        windSpeed = 0
        windGustSpeed = 0
        windGust = False
        lightning = False
        tempC = 0
        dewpointC = 0
        vis = 0
        altimHg = 0.0
        obs = ""
        skyConditions = []
        if metar.find('wind_gust_kt') is not None:
            windGustSpeed = int(metar.find('wind_gust_kt').text)
            windGust = (True if (ALWAYS_BLINK_FOR_GUSTS or windGustSpeed > CONFIGURATION['wind']['blink_threshold']) else False)
        if metar.find('wind_speed_kt') is not None:
            windSpeed = int(metar.find('wind_speed_kt').text)
        if metar.find('wind_dir_degrees') is not None:
            windDir = metar.find('wind_dir_degrees').text
        if metar.find('temp_c') is not None:
            tempC = int(round(float(metar.find('temp_c').text)))
        if metar.find('dewpoint_c') is not None:
            dewpointC = int(round(float(metar.find('dewpoint_c').text)))
        if metar.find('visibility_statute_mi') is not None:
            vis = int(round(float(metar.find('visibility_statute_mi').text)))
        if metar.find('altim_in_hg') is not None:
            altimHg = float(round(float(metar.find('altim_in_hg').text), 2))
        if metar.find('wx_string') is not None:
            obs = metar.find('wx_string').text
        if metar.find('observation_time') is not None:
            obsTime = datetime.datetime.fromisoformat(metar.find('observation_time').text.replace("Z","+00:00"))
        for skyIter in metar.iter("sky_condition"):
            skyCond = { "cover" : skyIter.get("sky_cover"), "cloudBaseFt": int(skyIter.get("cloud_base_ft_agl", default=0)) }
            skyConditions.append(skyCond)
        if metar.find('raw_text') is not None:
            rawText = metar.find('raw_text').text
            lightning = False if ((rawText.find('LTG', 4) == -1 and rawText.find('TS', 4) == -1) or rawText.find('TSNO', 4) != -1) else True
        print(stationId + ":" 
        + flightCategory + ":" 
        + str(windDir) + "@" + str(windSpeed) + ("G" + str(windGustSpeed) if windGust else "") + ":"
        + str(vis) + "SM:"
        + obs + ":"
        + str(tempC) + "/"
        + str(dewpointC) + ":"
        + str(altimHg) + ":"
        + str(lightning))
        conditionDict[stationId] = { "flightCategory" : flightCategory, "windDir": windDir, "windSpeed" : windSpeed, "windGustSpeed": windGustSpeed, "windGust": windGust, "vis": vis, "obs" : obs, "tempC" : tempC, "dewpointC" : dewpointC, "altimHg" : altimHg, "lightning": lightning, "skyConditions" : skyConditions, "obsTime": obsTime }
        if displayairports is None or stationId in displayairports:
            stationList.append(stationId)

    # Start up external display output
    disp = None
    if displaymetar is not None and ACTIVATE_EXTERNAL_METAR_DISPLAY:
        print("setting up external display")
        disp = displaymetar.startDisplay()
        displaymetar.clearScreen(disp)

    # Setting LED colors based on weather conditions
    looplimit = int(round(BLINK_TOTALTIME_SECONDS / BLINK_SPEED)) if (CONFIGURATION['wind']['enabled'] or CONFIGURATION['lightning']['enabled'] or ACTIVATE_EXTERNAL_METAR_DISPLAY) else 1

    windCycle = False
    displayTime = 0.0
    displayAirportCounter = 0
    numAirports = len(stationList)
    while looplimit > 0:
        i = 0
        for airportcode in airports:
            # Skip NULL entries
            if airportcode == "NULL":
                i += 1
                continue

            color = COLOR_CLEAR
            conditions = conditionDict.get(airportcode, None)
            windy = False
            highWinds = False
            lightningConditions = False

            if conditions != None:
                windy = True if (CONFIGURATION['wind']['enabled'] and windCycle == True and (conditions["windSpeed"] >= CONFIGURATION['wind']['blink_threshold'] or conditions["windGust"] == True)) else False
                highWinds = True if (windy and CONFIGURATION['wind']['high_wind_threshold'] != -1 and (conditions["windSpeed"] >= CONFIGURATION['wind']['high_wind_threshold'] or conditions["windGustSpeed"] >= CONFIGURATION['wind']['high_wind_threshold'])) else False
                lightningConditions = True if (CONFIGURATION['lightning']['enabled'] and windCycle == False and conditions["lightning"] == True) else False
                if conditions["flightCategory"] == "VFR":
                    color = CONFIGURATION['colours']['VFR'] if not (windy or lightningConditions) else CONFIGURATION['colours']['lightning'] if lightningConditions else CONFIGURATION['colours']['high_wind'] if highWinds else (fade_colour(CONFIGURATION['colours']['VFR']) if FADE_INSTEAD_OF_BLINK else COLOR_CLEAR) if windy else COLOR_CLEAR
                elif conditions["flightCategory"] == "MVFR":
                    color = CONFIGURATION['colours']['MVFR'] if not (windy or lightningConditions) else CONFIGURATION['colours']['lightning'] if lightningConditions else CONFIGURATION['colours']['high_wind'] if highWinds else (fade_colour(CONFIGURATION['colours']['MVFR']) if FADE_INSTEAD_OF_BLINK else COLOR_CLEAR) if windy else COLOR_CLEAR
                elif conditions["flightCategory"] == "IFR":
                    color = CONFIGURATION['colours']['IFR'] if not (windy or lightningConditions) else CONFIGURATION['colours']['lightning'] if lightningConditions else CONFIGURATION['colours']['high_wind'] if highWinds else (fade_colour(CONFIGURATION['colours']['IFR']) if FADE_INSTEAD_OF_BLINK else COLOR_CLEAR) if windy else COLOR_CLEAR
                elif conditions["flightCategory"] == "LIFR":
                    color = CONFIGURATION['colours']['LIFR'] if not (windy or lightningConditions) else CONFIGURATION['colours']['lightning'] if lightningConditions else CONFIGURATION['colours']['high_wind'] if highWinds else (fade_colour(CONFIGURATION['colours']['LIFR']) if FADE_INSTEAD_OF_BLINK else COLOR_CLEAR) if windy else COLOR_CLEAR
                else:
                    color = COLOR_CLEAR

            print("Setting LED " + str(i) + " for " + airportcode + " to " + ("lightning " if lightningConditions else "") + ("very " if highWinds else "") + ("windy " if windy else "") + (conditions["flightCategory"] if conditions != None else "None") + " " + str(color))
            pixels[i] = color
            i += 1

        # Legend
        if SHOW_LEGEND:
            pixels[i + OFFSET_LEGEND_BY] = CONFIGURATION['colours']['VFR']
            pixels[i + OFFSET_LEGEND_BY + 1] = CONFIGURATION['colours']['MVFR']
            pixels[i + OFFSET_LEGEND_BY + 2] = CONFIGURATION['colours']['IFR']
            pixels[i + OFFSET_LEGEND_BY + 3] = CONFIGURATION['colours']['LIFR']
            if CONFIGURATION['lightning']['enabled'] == True:
                pixels[i + OFFSET_LEGEND_BY + 4] = CONFIGURATION['colours']['lightning'] if windCycle else CONFIGURATION['colours']['VFR'] # lightning
            if CONFIGURATION['wind']['enabled'] == True:
                pixels[i+ OFFSET_LEGEND_BY + 5] = CONFIGURATION['colours']['VFR'] if not windCycle else (fade_colour(CONFIGURATION['colours']['VFR']) if FADE_INSTEAD_OF_BLINK else COLOR_CLEAR)    # windy
                if CONFIGURATION['wind']['high_wind_threshold'] != -1:
                    pixels[i + OFFSET_LEGEND_BY + 6] = CONFIGURATION['colours']['VFR'] if not windCycle else CONFIGURATION['colours']['high_wind']  # high winds

        # Update actual LEDs all at once
        pixels.show()

        # Rotate through airports METAR on external display
        if disp is not None:
            if displayTime <= DISPLAY_ROTATION_SPEED:
                displaymetar.outputMetar(disp, stationList[displayAirportCounter], conditionDict.get(stationList[displayAirportCounter], None))
                displayTime += BLINK_SPEED
            else:
                displayTime = 0.0
                displayAirportCounter = displayAirportCounter + 1 if displayAirportCounter < numAirports-1 else 0
                print("showing METAR Display for " + stationList[displayAirportCounter])

        # Switching between animation cycles
        time.sleep(BLINK_SPEED)
        windCycle = False if windCycle else True
        looplimit -= 1

    print()
    print("Done")

if __name__ == '__main__':
    dirname, scriptname = os.path.split(os.path.abspath(__file__))
    config_file = f'{dirname}{os.sep}config.json'
    if os.path.exists(config_file):
        CONFIGURATION = load_json(config_file)
    else:
        # Use defaults and save them for next run
        save_json(config_file, CONFIGURATION)

    print("Running metar.py at " + datetime.datetime.now().strftime('%d/%m/%Y %H:%M'))
    main()
